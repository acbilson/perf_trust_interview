FROM node:current-buster

RUN apt-get update && apt-get upgrade -y
RUN apt-get install vim -y

EXPOSE 4200
ENTRYPOINT ["tail", "-f", "/dev/null"]
